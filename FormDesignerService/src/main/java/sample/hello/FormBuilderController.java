package sample.hello;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

/**
 * Created by Think on 2017/7/6.
 */
@Controller
public class FormBuilderController {

//    @RequestMapping(value="/",method = RequestMethod.GET)
////    @ResponseBody
//    public String index(){
//        System.out.println("hahahaha");
//        return "index";
//    }
    @RequestMapping(value = "/",method = RequestMethod.GET)
    public ModelAndView index(){
        System.out.println("!!! model and view");
        return new ModelAndView("index");
    }

}
