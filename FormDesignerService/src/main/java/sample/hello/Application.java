package sample.hello;

/**
 * Created by Lipeilong on 2017/7/4.
 *
 * A Sample of RESTful web service with Spring.
 * Run this class,and visit http://localhost:8080/greeting
 *
 * ref:https://spring.io/guides/gs/rest-service/
 */
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Application {

    /**
     * A main method to start this application.
     */
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}