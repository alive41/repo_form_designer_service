# springboot-camel-drools-server

springboot-camel-drools-server

### Introduction

For integrating springboot, camel and drools, the nitial Source code of this project is copied from below open source projects:

(1). https://github.com/apache/camel/tree/master/examples/camel-example-spring-boot

(2). https://repository.jboss.org/nexus/content/repositories/releases/org/drools/drools-camel-server-example/6.2.0.CR4/

(3). https://github.com/jboss-integration/fuse-bxms-integ/tree/master/camel/kie-camel 

Content of build.gradle file is borrowed from the pom.xml files under link (1) and (2).

The resource file '/src/main/resources/META-INF/services/org/apache/camel/component/kie' under link (3) is also copied into this project.